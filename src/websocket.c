#include <gcrypt.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <libguile.h>
#include <errno.h>
#include <poll.h>

#include "log.h"

#define POLL_TIMEOUT 5000

#define WEBSOCK_CONT 0
#define WEBSOCK_TEXT 1
#define WEBSOCK_BINARY 2
#define WEBSOCK_CLOSE 8
#define WEBSOCK_PING 9
#define WEBSOCK_PONG 10

struct sha1 {
	int len;
	unsigned char hash[1];
	};

struct websock {
	int sock;
	int confirmed;
	SCM responder;
	};

scm_t_bits websock_tag;
static const char *encode_table =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char *websocket_magic =
		"258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
static int mod_table[] = {0, 2, 1};
static SCM websockets;
static SCM sock_mutex;
static int socket_count;

static struct sha1 *sha1_sum(const char *src) {
	int hash_length = gcry_md_get_algo_dlen(GCRY_MD_SHA1);
	struct sha1 *out;
	out = (struct sha1 *)malloc(sizeof(struct sha1) + hash_length);
	out->len = hash_length;
	gcry_md_hash_buffer(GCRY_MD_SHA1, out->hash, src, strlen(src));
	return out;
	}

static char *base64_encode(const unsigned char *src, size_t inlen) {
	size_t outlen;
	outlen = 4 * ((inlen + 2) / 3);
	char *encoded = (char *)malloc(outlen + 1);
	int i = 0, j = 0;
	uint32_t a, b, c, triple;
	while (i < inlen) {
		a = (i < inlen ? src[i++] : 0);
		b = (i < inlen ? src[i++] : 0);
		c = (i < inlen ? src[i++] : 0);
		triple = (a << 16) + (b << 8) + c;
		encoded[j++] = encode_table[(triple >> 3 * 6) & 0x3f];
		encoded[j++] = encode_table[(triple >> 2 * 6) & 0x3f];
		encoded[j++] = encode_table[(triple >> 1 * 6) & 0x3f];
		encoded[j++] = encode_table[(triple >> 0 * 6) & 0x3f];
		}
	for (i = 0; i < mod_table[inlen % 3]; i++)
		encoded[outlen - 1 - i] = '=';
	encoded[outlen] = '\0';
	return encoded;
	}

char *websocket_accept(const char *ws_key) {
	char *cat, *b64;
	struct sha1 *sum;
	cat = (char *)malloc(strlen(ws_key) + strlen(websocket_magic) + 1);
	strcpy(cat, ws_key);
	strcat(cat, websocket_magic);
	sum = sha1_sum(cat);
	free(cat);
	b64 = base64_encode(sum->hash, sum->len);
	free(sum);
	return b64;
	}

static void pull_bad_sock(int sock, int close_it) {
	/* Remove from websockets a socket that has apparently
	   been closed or has otherwise failed. */
	SCM node = websockets;
	SCM newlist = SCM_EOL;
	SCM group;
	SCM newgroup = SCM_EOL;
	SCM item = SCM_EOL;
	struct websock *ws;
	scm_lock_mutex(sock_mutex);
	log_msg("pull bad sock: %d\n", sock);
	while (node != SCM_EOL) {
		group = SCM_CAR(node);
		newgroup = SCM_EOL;
		while (group != SCM_EOL) {
			item = SCM_CAR(group);
			if (SCM_SMOB_PREDICATE(websock_tag, item)) {
				ws = (struct websock *)SCM_SMOB_DATA(item);
				if (ws->sock == sock) {
					log_msg("websocket %d removed\n", sock);
					if (close_it) close(sock);
					socket_count -= 1;
					}
				else newgroup = scm_cons(item, newgroup);
				}
			else newgroup = scm_cons(item, newgroup);
			group = SCM_CDR(group);
			}
		newlist = scm_cons(newgroup, newlist);
		node = SCM_CDR(node);
		}
	scm_remember_upto_here_2(newgroup, group);
	scm_remember_upto_here_2(node, item);
	if (newlist != SCM_EOL) scm_gc_protect_object(newlist);
	if (websockets != SCM_EOL) scm_gc_unprotect_object(websockets);
	websockets = newlist;
	scm_c_define("websockets", websockets);
	scm_remember_upto_here_1(newlist);
	scm_unlock_mutex(sock_mutex);
	return;
	}

static uint64_t send_block(int sock,
		const unsigned char *msg, uint64_t len, int *fail) {
	uint64_t sent, n;
	sent = 0;
	*fail = 0;
	while (sent < len) {
		n = send(sock, msg + sent, len - sent, 0);
		if (n < 0) {
			log_msg("websocket-send error on socket %d: %s\n",
				sock, strerror(errno));
			pull_bad_sock(sock, 1);
			*fail = 1;
			break;
			}
		sent += n;
		}
	return sent;
	}

static uint64_t websocket_send(int sock, int opcode,
		const unsigned char *payload, uint64_t payload_len) {
	uint64_t total_sent = 0;
	unsigned char header[14];
	uint64_t header_len;
	int fail;
	memset(header, 0, sizeof(header));
	header[0] = 0x80 + opcode;
	if (payload_len < 126) {
		header[1] = payload_len;
		header_len = 2;
		}
	else if (payload_len < 65536) {
		header[1] = 126;
		header[2] = (payload_len >> 8) && 0xff;
		header[3] = payload_len && 0xff;
		header_len = 4;
		}
	else {
		header[1] = 127;
		int i;
		for (i = 9; i >= 2; i--) {
			header[i] = payload_len && 0xff;
			payload_len >>= 8;
			}
		header_len = 10;
		}
	total_sent += send_block(sock, header, header_len, &fail);
	log_msg("sent header %d, fail %d\n", header_len, fail);
	if (!fail && (payload_len > 0)) {
		total_sent += send_block(sock, payload, payload_len, &fail);
		log_msg("sent payload %d, fail %d\n", payload_len, fail);
		}
	return total_sent;
	}

static SCM websocket_send_text(SCM channel, SCM payload) {
	SCM key = scm_string_to_symbol(channel);
	scm_lock_mutex(sock_mutex);
	SCM socklist = scm_assq_ref(websockets, key);
	scm_unlock_mutex(sock_mutex);
	scm_remember_upto_here_1(key);
	if (socklist == SCM_BOOL_F) {
		char *chan = scm_to_locale_string(channel);
		log_msg("websocket-send: no such channel '%s'\n", chan);
		free(chan);
		return SCM_UNSPECIFIED;
		}
	char *msg = scm_to_utf8_string(payload);
log_msg("send: %s\n", msg);
	scm_remember_upto_here_2(channel, payload);
	struct websock *ws;
	while (socklist != SCM_EOL) {
		ws = (struct websock *)SCM_SMOB_DATA(SCM_CAR(socklist));
		if (ws->confirmed)  {
			websocket_send(ws->sock, WEBSOCK_TEXT,
				(unsigned char *)msg, strlen(msg));
			}
		socklist = SCM_CDR(socklist);
		}
	free(msg);
log_msg("done\n");
	scm_remember_upto_here_1(socklist);
	return SCM_UNSPECIFIED;
	}

static SCM websocket_send_ping(SCM channel, SCM payload) {
	SCM key = scm_string_to_symbol(channel);
	scm_lock_mutex(sock_mutex);
	SCM socklist = scm_assq_ref(websockets, key);
	scm_unlock_mutex(sock_mutex);
	scm_remember_upto_here_1(key);
	if (socklist == SCM_BOOL_F) {
		char *chan = scm_to_locale_string(channel);
		log_msg("websocket-ping: no such channel '%s'\n", chan);
		free(chan);
		return SCM_UNSPECIFIED;
		}
	char *msg = scm_to_utf8_string(payload);
	if (strlen(msg) > 125) msg[125] = '\0';
	scm_remember_upto_here_2(channel, payload);
	struct websock *ws;
	while (socklist != SCM_EOL) {
		ws = (struct websock *)SCM_SMOB_DATA(SCM_CAR(socklist));
		if (ws->confirmed) {
			websocket_send(ws->sock, WEBSOCK_PING,
				(unsigned char *)msg, strlen(msg));
			}
		socklist = SCM_CDR(socklist);
		}
	free(msg);
	scm_remember_upto_here_1(socklist);
	return SCM_UNSPECIFIED;
	}

static uint64_t websocket_send_pong(int sock, const char *payload) {
	return websocket_send(sock, WEBSOCK_PONG,
					(unsigned char *)payload, strlen(payload));
	}

static uint64_t websocket_send_close(int sock,
						unsigned const char *payload, uint64_t len) {
	pull_bad_sock(sock, 0);
	return websocket_send(sock, 8, payload, len);
	}

static SCM mark_websock(SCM smob) {
	struct websock *ws;
	ws = (struct websock *)SCM_SMOB_DATA(smob);
	if (ws->responder != SCM_BOOL_F) scm_gc_mark(ws->responder);
	return SCM_UNSPECIFIED;
	}

void init_websockets(void) {
	websockets = SCM_EOL;
	socket_count = 0;
	websock_tag = scm_make_smob_type("websocket", sizeof(struct websock));
	scm_set_smob_mark(websock_tag, mark_websock);
	scm_c_define("websockets", websockets);
	scm_permanent_object(sock_mutex = scm_make_mutex());
	scm_c_define_gsubr("websocket-send", 2, 0, 0, websocket_send_text);
	scm_c_define_gsubr("websocket-ping", 2, 0, 0, websocket_send_ping);
	}

void shutdown_websockets(void) {
	SCM node, sockets;
	node = websockets;
	struct websock *ws;
	while (node != SCM_EOL) {
		sockets = SCM_CDR(SCM_CAR(node));
		while (sockets != SCM_EOL) {
			ws = (struct websock *)SCM_SMOB_DATA(SCM_CAR(sockets));
			close(ws->sock);
			sockets = SCM_CDR(sockets);
			}
		node = SCM_CDR(node);
		}
	scm_remember_upto_here_2(node, sockets);
	return;
	}

static struct pollfd *make_pollset() {
	struct pollfd *polls;
	polls = (struct pollfd *)malloc(sizeof(struct pollfd) * socket_count);
	SCM node = websockets;
	SCM sockets;
	struct websock *ws;
	while (node != SCM_EOL) {
		sockets = SCM_CDR(SCM_CAR(node));
		int i = 0;
		while (sockets != SCM_EOL) {
			ws = (struct websock *)SCM_SMOB_DATA(SCM_CAR(sockets));
			polls[i].fd = ws->sock;
			polls[i].events = POLLIN | POLLHUP;
			i++;
			sockets = SCM_CDR(sockets);
			}
		node = SCM_CDR(node);
		}
	scm_remember_upto_here_2(node, sockets);
	return polls;
	}

static int read_all(int sock, void* buf, size_t n, const char *tag) {
	size_t i;
	ssize_t k;
	for (i = 0; i < n; ) {
		k = read(sock, buf + i, n - i);
		if (k < 0) {
			log_msg("read on sock %d/%s failed: %s\n",
				sock, tag, strerror(errno));
			return 0;
			}
		i += k;
		}
	return 1;
	}

static struct websock *find_websocket(int sock) {
	SCM node, sockets;
	struct websock *ws;
	node = websockets;
	while (node != SCM_EOL) {
		sockets = SCM_CAR(node);
		while (sockets != SCM_EOL) {
			ws = (struct websock *)SCM_SMOB_DATA(SCM_CAR(sockets));
			if (ws->sock == sock) return ws;
			sockets = SCM_CDR(sockets);
			}
		node = SCM_CDR(node);
		}
	scm_remember_upto_here_2(node, sockets);
	return NULL;
	}

static void set_confirmed(int sock) {
	struct websock *ws;
	ws = find_websocket(sock);
	log_msg("seek socket %d %s\n", sock,
		(ws == NULL ? "FAIL" : "SUCCESS"));
	if (ws != NULL) {
		ws->confirmed = 1;
		log_msg("CONFIRM socket %d\n", sock);
		}
	return;
	}

static SCM find_responder(int sock) {
	struct websock *ws;
	ws = find_websocket(sock);
	return (ws == NULL ? SCM_BOOL_F : ws->responder);
	}

static void read_msg(int sock) {
	unsigned char header[12];
	unsigned char *payload;
	uint64_t payload_len;
	unsigned char mask[4];
	int masked, opcode, i;
	if (!read_all(sock, header, 2, "hdr")) {
		pull_bad_sock(sock, 1);
		return;
		}
	log_msg("WSFRAME 0: %02x\n", header[0]);
	log_msg("WSFRAME 1: %02x\n", header[1]);
	opcode = header[0] & 0xf;
	SCM responder = find_responder(sock);
	const char *opcode_name;
	switch (opcode) {
	case WEBSOCK_CONT:
		opcode_name = "continuation";
		break;
	case WEBSOCK_TEXT:
		opcode_name = "text";
		break;
	case WEBSOCK_BINARY:
		opcode_name = "binary";
		break;
	case WEBSOCK_CLOSE:
		opcode_name = "close";
		break;
	case WEBSOCK_PING:
		opcode_name = "ping";
		break;
	case WEBSOCK_PONG:
		opcode_name = "pong";
		break;
	default:
		opcode_name = "unknown";
		};
	log_msg("WSFRAME OPCODE: %s\n", opcode_name);
	payload_len = (uint64_t)(header[1] & 0x7f);
	masked = header[1] & 0x80;
	if (payload_len == 126) {
		read_all(sock, header, 2, "len126");
		payload_len = (header[0] << 8) + header[1];
		}
	else if (payload_len == 127) {
		read_all(sock, header, 8, "len127");
		payload_len = 0;
		for (i = 0; i < 8; i++) {
			payload_len = (payload_len << 8) + header[i];
			}
		}
	log_msg("WSFRAME PAYLOAD %d\n", payload_len);
	if (masked) {
		read_all(sock, mask, 4, "mask");
		log_msg("WSFRAME MASK: %08x\n", mask);
		}
	payload = (unsigned char *)malloc(payload_len + 1);
	read_all(sock, payload, payload_len, "payload");
	payload[payload_len] = '\0';
	if (masked) {
		for (i = 0; i < payload_len; i++)
			payload[i] = payload[i] ^ mask[i % 4];
		}
	log_msg("WSFRAME PAYLOAD: %s\n", (char *)payload);
	if ((opcode == WEBSOCK_TEXT) &&
			(strcmp((char *)payload, "*CONFIRM*") == 0)) {
		set_confirmed(sock);
		}
	if (opcode == WEBSOCK_PING) {
		log_msg("WSFRAME REPLY PONG\n");
		websocket_send_pong(sock, (char *)payload);
		}
	else if (opcode == WEBSOCK_CLOSE) {
		log_msg("WSFRAME REPLY CLOSE\n");
		websocket_send_close(sock, payload, payload_len);
		}
	if ((responder != SCM_BOOL_F) && (opcode != WEBSOCK_CLOSE)) {
		scm_call_2(responder, scm_from_locale_symbol(opcode_name),
			scm_from_utf8_string((char *)payload));
		}
	free(payload);
	scm_remember_upto_here_1(responder);
	return;
	}

static SCM websock_thread(void *data) {
	struct pollfd *polls;
	int count_cache, i, res;
	log_msg("launch websocket thread\n");
	while (1) {
		scm_lock_mutex(sock_mutex);
		if (socket_count == 0) {
			scm_unlock_mutex(sock_mutex);
			log_msg("no websockets; exit thread\n");
			break;
			}
		count_cache = socket_count;
		polls = make_pollset();
		scm_unlock_mutex(sock_mutex);
		while (count_cache == socket_count) {
			res = poll(polls, socket_count, POLL_TIMEOUT);
			if (res == 0) continue;
			if (res < 0) {
				log_msg("websock poll: %s\n", strerror(errno));
				continue;
				}
			for (i = 0; i < socket_count; i++) {
log_msg("check sock %d: %08x\n", polls[i].fd, polls[i].revents);
				if (polls[i].revents & POLLHUP) {
					pull_bad_sock(polls[i].fd, 1);
					}
				else if (polls[i].revents & POLLIN) {
					read_msg(polls[i].fd);
					}
				}
			}
		free(polls);
		}
	return SCM_BOOL_T;
	};

void add_websocket(SCM channel, int sock, SCM responder) {
	if (channel == SCM_BOOL_F) {
		log_msg("add_websocket: no channel ID for socket %d\n", sock);
		close(sock);
		return;
		}
	scm_lock_mutex(sock_mutex);
	SCM key = scm_string_to_symbol(channel);
	char *s = scm_to_locale_string(channel);
	log_msg("add socket %d to '%s'\n", sock, s);
	free(s);
	scm_remember_upto_here_1(channel);
	SCM socklist = scm_assq_ref(websockets, key);
	if (socklist == SCM_BOOL_F) socklist = SCM_EOL;
	struct websock *ws =
		(struct websock *)scm_gc_malloc(sizeof(struct websock),
					"websocket");
	SCM smob;
	ws->sock = sock;
	ws->confirmed = 0;
	ws->responder = responder;
	scm_remember_upto_here_1(responder);
	SCM_NEWSMOB(smob, websock_tag, ws);
	SCM newlist = scm_cons(smob, socklist);
	scm_remember_upto_here_1(smob);
	newlist = scm_assq_set_x(websockets, key, newlist);
	scm_remember_upto_here_2(key, socklist);
	if (websockets != SCM_EOL) scm_gc_unprotect_object(websockets);
	scm_gc_protect_object(newlist);
	websockets = newlist;
	scm_remember_upto_here_1(newlist);
	scm_c_define("websockets", websockets);
	scm_unlock_mutex(sock_mutex);
	socket_count += 1;
	if (socket_count == 1) {
		scm_spawn_thread(websock_thread, NULL, NULL, NULL);
		}
	return;
	}
