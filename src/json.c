/*
** Copyright (c) 2013 Peter Yadlowsky <pmy@virginia.edu>
**
** This program is free software ; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation ; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY ; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program ; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <libguile.h>
#include <stdio.h>

#include "log.h"
#include "gtime.h"
#include "jsmn.h"

static char *make_key(SCM obj) {
	SCM string;
	char *key;
	if (scm_is_string(obj)) string = obj;
	else string = scm_symbol_to_string(obj);
	key = scm_to_utf8_string(string);
	scm_remember_upto_here_2(obj, string);
	return key;
	}

static void json_build(SCM obj, FILE *stream) {
	SCM node;
	char *buf;
	if (scm_is_string(obj)) {
		size_t len, i;
		fputc('"', stream);
		buf = scm_to_utf8_stringn(obj, &len);
		for (i = 0; i < len; i++) {
			if (buf[i] == '"') fputs("\\\"", stream);
			else if (buf[i] == '\\') fputs("\\\\", stream);
			else if (buf[i] == '\n') fputs("\\n", stream);
			else if (buf[i] == '\r') fputs("\\r", stream);
			else if (buf[i] == '\t') fputs("\\t", stream);
			else fputc(buf[i], stream);
			}
		free(buf);
		fputc('"', stream);
		}
	else if (scm_is_symbol(obj)) {
		buf = scm_to_utf8_string(scm_symbol_to_string(obj));
		fprintf(stream, "\"%s\"", buf);
		free(buf);
		}
	else if (scm_boolean_p(obj) == SCM_BOOL_T) {
		if (obj == SCM_BOOL_T) {
			fprintf(stream, "true");
			}
		else {
			fprintf(stream, "false");
			}
		}
	else if (scm_is_number(obj)) {
		if (scm_is_exact(obj)) {
			fprintf(stream, "%d", scm_to_int(obj));
			}
		else {
			fprintf(stream, "%1.20e", scm_to_double(obj));
			}
		}
	else if (scm_is_null(obj)) {
		fprintf(stream, "null");
		}
	else if ((scm_list_p(obj) == SCM_BOOL_T) &&
			(!scm_is_null(obj)) &&
			(scm_pair_p(SCM_CAR(obj)) == SCM_BOOL_T) &&
			scm_is_symbol(SCM_CAR(SCM_CAR(obj)))) {
		char *key;
		int need_comma = 0;
		SCM pair;
		pair = SCM_EOL;
		fprintf(stream, "{");
		for (node = obj; node != SCM_EOL; node = SCM_CDR(node)) {
			pair = SCM_CAR(node);
			key = make_key(SCM_CAR(pair));
			fprintf(stream, "%s\"%s\":", (need_comma ? "," : ""), key);
			json_build(SCM_CDR(pair), stream);
			free(key);
			need_comma = 1;
			}
		fprintf(stream, "}");
		scm_remember_upto_here_1(pair);
		}
	else if (scm_list_p(obj) == SCM_BOOL_T) {
		int need_comma = 0;
		fprintf(stream, "[");
		for (node = obj; node != SCM_EOL; node = SCM_CDR(node)) {
			if (need_comma) fprintf(stream, ",");
			json_build(SCM_CAR(node), stream);
			need_comma = 1;
			}
		fprintf(stream, "]");
		}
	else if (SCM_SMOB_PREDICATE(time_tag, obj)) {
		buf = scm_to_utf8_string(format_time(obj,
			scm_from_utf8_string("%Y-%m-%d %H:%M:%S")));
		fprintf(stream, "\"%s\"", buf);
		free(buf);
		}
	else {
		fprintf(stream, "null");
		}
	scm_remember_upto_here_2(obj, node);
	return;
	}

/*
static void discard(json_t *obj) {
	int n;
	if (obj == NULL) return;
	if ((n = obj->refcount) > 0)
		while (n--) json_decref(obj);
	return;
	}
*/
SCM json_encode(SCM obj) {
	char *mbuf;
	size_t len;
	SCM out;
	FILE *stream;
	stream = open_memstream(&mbuf, &len);
	json_build(obj, stream);
	scm_remember_upto_here_1(obj);
	fclose(stream);
	out = scm_from_utf8_stringn(mbuf, len);
	free(mbuf);
	return out;
	scm_remember_upto_here_1(out);
	}

static SCM parse(char *src, jsmntok_t *tokens, int *cursor) {
	jsmntok_t *token;
	SCM obj;
	int i;
	token = &tokens[*cursor];
	int start = token->start;
	size_t len = token->end - token->start;
	obj = SCM_EOL;
	if (token->type == JSMN_STRING) {
		obj = scm_from_utf8_stringn(&src[start], len);
		}
	else if (token->type == JSMN_PRIMITIVE) {
		if (src[start] == 't') obj = SCM_BOOL_T;
		else if (src[start] == 'f') obj = SCM_BOOL_F;
		else if (src[start] == 'n') obj = SCM_EOL;
		else {
			char c = src[token->end];
			src[token->end] = '\0';
			if (index(&src[start], '.') == NULL)
				obj = scm_from_int(atoi(&src[start]));
			else obj = scm_from_double(atof(&src[start]));
			src[token->end] = c;
			}
		}
	else if (token->type == JSMN_OBJECT) {
		obj = SCM_EOL;
		SCM key = SCM_EOL, value = SCM_EOL;
		for (i = 0; i < token->size; i++) {
			*cursor += 1;
			key = scm_string_to_symbol(parse(src, tokens, cursor));
			*cursor += 1;
			value = parse(src, tokens, cursor);
			obj = scm_cons(scm_cons(key, value), obj);
			}
		obj = scm_reverse(obj);
		scm_remember_upto_here_2(key, value);
		}
	else if (token->type == JSMN_ARRAY) {
		obj = SCM_EOL;
		for (i = 0; i < token->size; i++) {
			*cursor += 1;
			obj = scm_cons(parse(src, tokens, cursor), obj);
			}
		obj = scm_reverse(obj);
		}
	return obj;
	scm_remember_upto_here_1(obj);
	}

SCM json_decode(SCM string) {
	char *buf;
	jsmn_parser parser;
	jsmntok_t *tokens;
	buf = scm_to_utf8_string(string);
	scm_remember_upto_here_1(string);
	jsmn_init(&parser);
	int n = jsmn_parse(&parser, buf, strlen(buf), NULL, 0);
	if (n < 0) {
		if (n == JSMN_ERROR_INVAL)
			log_msg("JSON decode: invalid JSON source\n");
		else if (n == JSMN_ERROR_PART)
			log_msg("JSON decode: premature source end\n");
		else log_msg("JSON decode error: %d\n", n);
		return SCM_BOOL_F;
		}
	jsmn_init(&parser);
	tokens = (jsmntok_t *)malloc(sizeof(jsmntok_t) * n);
	jsmn_parse(&parser, buf, strlen(buf), tokens, n);
	int cursor = 0;
	SCM obj = parse(buf, tokens, &cursor);
	free(buf);
	free(tokens);
	return obj;
	scm_remember_upto_here_1(obj);
	}

void init_json(void) {
	scm_c_define_gsubr("json-encode", 1, 0, 0, json_encode);
	scm_c_define_gsubr("json-decode", 1, 0, 0, json_decode);
	}
