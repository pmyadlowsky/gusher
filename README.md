gusher
======

Gusher is a multi-threaded
web application server written in C and embedding the
GNU [Guile](http://www.gnu.org/software/guile/) extension environment
to host applications written in
[Scheme](https://en.wikipedia.org/wiki/Scheme_%28programming_language%29).
Each Gusher web application runs as a free-standing web server,
though will typically 
be proxied by a general-purpose web server such as apache or nginx.
Features include:

- basic [postgresql](http://www.postgresql.org/) interface for SQL
database access

- http client implemented with [libcurl](http://curl.haxx.se/), for
fetching material (e.g. RSS content) from other web sources

- JSON encode/decode data serialization to facilitate AJAX
transactions and to transact JSON-encoded documents

- XML parsing with [libxml2](http://xmlsoft.org/), for receiving and
generating RSS feeds and the like

- WebSockets support

- [make](http://linux.die.net/man/1/make)-like content caching facility;
auto-refreshes cached items in response to changes in their dependencies

- fast, simple templating facility, for composing HTML, complex
SQL queries, anything that can be represented as a template

- asynchronous intra- and inter-application publish-subscribe messaging
implemented with http

- simple cron-like job scheduling facility

- simple SMTP client for sending email

- command line editing and history (libreadline) for interactive
Scheme coding
