; HTTP responders

(define-module (gusher responders)
	#:use-module (guile-user)
	#:use-module (gusher misc)
	#:export
		(http-html http-xml http-text http-json http-websocket
			request-cache))

(define (http-html path responder)
	; HTML response
	(http path
		(lambda (req)
			(simple-response "text/html; charset=UTF-8" (responder req)))))
(define (http-xml path responder)
	; XML responsee
	(http path
		(lambda (req)
			(simple-response "text/xml; charset=UTF-8" (responder req)))))
(define (http-text path responder)
	; plain text response
	(http path
		(lambda (req)
			(simple-response "text/plain; charset=UTF-8" (responder req)))))
(define (http-json path responder)
	; JSON response
	(http path
		(lambda (req)
			(let ([response (responder req)])
				(json-response
					(if (or (string? response) (zdeflate? response))
						response (json-encode response)))))))
(define (http-websocket path responder)
	; websocket response
	(http path
		(lambda (req)
			(let ([key (assq-ref req 'sec-websocket-key)])
				(unless key
					(log-msg "websocket request missing key: ~s" path))
				(websocket-response key (responder req))))))
(define (request-cache query)
	; Create fluid and accessor for per-request caching
	; of some aspect of http request.
	; Call passed query function once to set the cache.
	; ex:
	;	(define env-authorizations
	;		(request-cache
	;			(lambda (req) (session-get req 'authorizations))))
	(let ([cache (make-unbound-fluid)])
		(lambda ()
			(unless (fluid-bound? cache)
				(fluid-set! cache (query (fluid-ref env-http-request))))
			(fluid-ref cache))))
