; postgres helpers
(define-module (gusher postgres)
	#:use-module (guile-user)
	#:use-module (gusher misc)
	#:export
		(pg-query pg-exec pg-one-row pg-open configure-database
			pg-connection))

(define (pg-query dbh query args)
	; safely plug SQL query parameters into query template,
	; to be used with postgres calls below
	(apply format
		(append (list #f query)
			(map (lambda (item) (pg-format dbh item)) args))))
(define (pg-exec dbh query . args)
	(pg-exec-primitive dbh
		(if (null? args) query (pg-query dbh query args))))
(define (pg-one-row dbh query . args)
	(pg-one-row-primitive dbh
		(if (null? args) query (pg-query dbh query args))))

; database configuration
(define *database-connection-profile* "")
(define* (pg-open #:optional (profile *database-connection-profile*))
	(pg-open-primitive profile))
(define configure-database
	(let ([query
				(deflate "create table if not exists _kv_ (
					namespace varchar,
					key varchar,
					value text,
					unique (namespace, key))")])
		(lambda (profile)
			(set! *database-connection-profile* profile)
			(let ([dbh (pg-open)])
				(pg-exec dbh query)
				(pg-close dbh)))))
(define (pg-connection profile)
	; Create fluid to cache one DB connection, return accessor.
	; The first time the accessor is called, it opens and caches
	; a DB connection in the current dynamic state, using the provided
	; DB profile. This connection is also cached in fluid
	; "pg-connections" for auto-disposal at conclusion of
	; the request process.
	; ex: (define dbh (pg-connection "dbname=mydb user=dbuser"))
	(let ([conn (make-unbound-fluid)]
			[dyn-state (make-fluid (current-dynamic-state))])
		(lambda ()
			(unless (equal? (fluid-ref dyn-state) (current-dynamic-state))
				; when dynamic state changes, force new DB connection
				; so child state doesn't use parent's connection
				(fluid-set! dyn-state (current-dynamic-state))
				(fluid-unset! conn))
			(unless (fluid-bound? conn)
				(let ([new-conn (pg-open-primitive profile)])
					(fluid-set! pg-connections
						(cons new-conn (fluid-ref pg-connections)))
					(fluid-set! conn new-conn))) (fluid-ref conn))))
