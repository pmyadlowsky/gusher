#! /usr/local/bin/gusher -d
!#

(define html "
<html>
<head>
<title>Gusher!</title>
</head>
<body>
I am Gusher!
<script type=\"text/javascript\">
	console.log(\"host \" + window.location.host);
	var sock = new WebSocket(\"ws://\" + window.location.host + \"/chat\");
	sock.onerror = function(event) {
		console.log(\"ERROR \" + JSON.stringify(event));
		};
	sock.onopen = function(event) {
		console.log(\"OPEN \" + JSON.stringify(event));
		console.log(\"PROTOCOL \" + sock.protocol);
		console.log(\"STATE \" + sock.readyState);
		console.log(\"URL \" + sock.url);
		sock.send(\"*CONFIRM*\");
		};
	sock.onclose = function(event) {
		console.log(\"CLOSE \" + JSON.stringify(event));
		console.log(\"code \" + event.code);
		console.log(\"reason \" + event.reason);
		};
	sock.onmessage = function(event) {
		console.log(\"DATA \" + JSON.stringify(JSON.parse(event.data)));
		console.log(\"ORIGIN \" + event.origin);
		};
</script>
</body>
</html>
")

(define foo "
<html>
<head>
<title>Gusher!</title>
</head>
<body>
FOO!
</body>
</html>
")

;(define home-t (cache-get-file "/home/pmy/gusher/home.html"))

(http-html "/index"
	(lambda (req)
(log-msg "REQ ~s" req)
			; (assq-ref req 'session)
			html
		)
	)
(http-websocket "/chat"
	(lambda (req)
		(lambda (opcode payload)
			(log-msg "RESPOND ~s ~s" opcode payload)
			)
		)
	)
(http-html "/foo"     (lambda (req) (assq-ref req 'query-string)))
(http-html "/foo/bar" (lambda (req) "BAR!"))
(http-text "/text"    (lambda (req) "just some text"))
(http-json "/json"    (lambda (req) (assq-ref req 'query)))
